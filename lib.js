(!code:start)
function(jtsTracker,oGateConfig) {

    this.oConf = oGateConfig;
    this.oTracker = jtsTracker;
    this.sTrackerName = "jTracker_" + Math.random().toString(36).substr(2, 5);

    this.init = function(){
        this.jtsFunctions = this.jtsFunctions || {};

        // Tracker executes the rawUserCallback functions
        window.jtsNamespace = window.jtsNamespace || {};
        window.jtsNamespace.jtsCallbacks = window.jtsNamespace.jtsCallbacks || {};
        window.jtsNamespace.jtsCallbacks.rawUserCallback = window.jtsNamespace.jtsCallbacks.rawUserCallback || [];
        
        this.jtsFunctions.trackJUIDToGaAProperty = function(iUserID, bNewUser)
        {
            var tracker = window.ga.getAll()[0];
            if(
                typeof tracker !== "undefined" &&
                typeof tracker.get !== "undefined"
            )
            {
                // Read J-Cookie
                var sCookieValue = this.oTracker.readCookie(this.oConf.cookieName);
                var sOriginalCID = tracker.get('clientId');

                // If not available or value is not equal or new user
                if(
                    sCookieValue === null ||
                    (
                        sCookieValue !== null &&
                        (
                            sOriginalCID !== sCookieValue ||
                            bNewUser === true
                        )
                    )
                )
                {
                    if(
                        typeof this.oConf.gaPropertyA !== "undefined" &&
                        typeof this.oConf.gaJUIDDimension !== "undefined"
                    ) {
                        var oParams = {};
                        oParams["nonInteraction"] = true;
                        oParams["dimension"+this.oConf.gaJUIDDimension] = iUserID;

                        // create a new tracker to GA account with temporary unique name
                        window.ga("create",this.oConf.gaPropertyA,"auto",this.sTrackerName);
                        // parameters: (namedTracker, hitType, eventCategory, eventAction, eventLabel, fieldObjects)
                        window.ga(this.sTrackerName + ".send","event","JENTIS","j-uid",iUserID,oParams);

                        this.oTracker.setCookie({
                            "name": this.oConf.cookieName,
                            "value": sOriginalCID,
                            "exdays": 7
                        });
                    }
                }


            }
        }.bind(this);

        window.jtsNamespace.jtsCallbacks.rawUserCallback.push(
            function(iUserID, bNewUser) {
                try {
                    if(typeof window.ga !== "undefined" && typeof ga.getAll !== "undefined")
                    {
                        this.jtsFunctions.trackJUIDToGaAProperty(iUserID, bNewUser);
                    }
                    else
                    {
                        var iIntervalJUIDCounter = 0;
                        this.intervalJUID = window.setInterval(function()
                        {
                            if(typeof window.ga !== "undefined" && typeof ga.getAll !== "undefined")
                            {
                                window.clearInterval(this.intervalJUID);
                                
                                this.jtsFunctions.trackJUIDToGaAProperty(iUserID, bNewUser);
                            } else {
                                iIntervalJUIDCounter++;

                                if(iIntervalJUIDCounter > 4) {
                                    window.clearInterval(this.intervalJUID);
                                }
                            }
                        }.bind(this),1000);
                    }
                } catch(ex)
                {

                }
            }.bind(this)
        );
    }
    
    this.init();
}
(!code:end)